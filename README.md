# REACT COMPONENT STORE CLIENT README

This is the react component store extension for visual studio code.

The purpose of this plugin is to make an easy to use storage solution for reusable react components.

## Features

The ability to retrieve react components in your code, no commands required.

## Commands

Type GET inside your code to view components that can be retrieved

Type CTRL+SHIFT+P to enter the next to commands
* `Push Component`:     To push a component to the server, the javascript filename will be used 
* `Update Component`:   Update the current component you are inside of

## Requirements

This extension requires the React Component Store Server in order to work.

## Extension Settings

In order to get this extension to work you will have to provide the correct IP address and
port for your server.

Additionally you may configure the location that the components are stored to by default.

This extension contributes the following settings:

* `reactComponentStore.Server`: The ip and port of your server using the http protocol, it is 'http://localhost:3000' by default
* `reactComponentStore.SaveComponentsDirectory`: the directory your components will be stored in, 'store_components' by default

## Known issues

* Not so much of an issue but currently it will only work with javascript files, you should adjust the source for your own use

### 1.0.0

Initial release of React Component Store with the ability to GET, PUSH, and UPDATE