/* -----------------------------
 * React Component Store Client
 * @Author:     Rupert Ben Wiser
 * @Version:    1.0.1
 * @Date:       16 June 2017
 * ----------------------------*/

'use strict';
import * as vscode from 'vscode';
import {CompletionItem, Range, Position, WorkspaceEdit, TextEditorSelectionChangeEvent} from 'vscode';
import axios from 'axios';
import * as fs from 'fs';

// This is the keyword used to get a component.

// Because the PUSH and UPDATE commands are commands and are required to be defined in the
// package.json file they cannot be variables like GET is.

const GET_KEYWORD: string = "GET";

// Where the settings are managed.

let {Server, SaveComponentsDirectory} = vscode.workspace.getConfiguration().reactComponentStore;
let SERVER = Server;
let SAVE_COMPONENT_DIRECTORY = SaveComponentsDirectory;

vscode.workspace.onDidChangeConfiguration(() => {
    let {Server, SaveComponentsDirectory} = vscode.workspace.getConfiguration().reactComponentStore;
    SERVER = Server;
    SAVE_COMPONENT_DIRECTORY = SaveComponentsDirectory;
});

// This is a hash which is retrieved from the server of the suggestions
// the server hashes the current suggestions and sends them over to the client.

// If they have changed the client updates it's list of suggestions.

let currentVersion: string = "";
let suggestionDisposable: vscode.Disposable = null;
let components: {name: string, propertyList: string[]}[] = [];

const checkVersion = async () => {
    let {data} = await axios.get(`${SERVER}/version`);

    if ( data !== currentVersion ) {
        currentVersion = data;
        getSuggestions();
    }

    setTimeout(checkVersion, 500);
}

// This is a list which during the extensions life time lets it keep track of which extensions have
// already been imported; it is initially filled by looking in the default extension directory.

// The reason why this is done is so that when an extension has not been imported yet, the
// code can also be retrieved from the server.

let usedComponents: string[] = [];

function getUsedComponents() {
    fs.readdir(`${vscode.workspace.rootPath}/${SAVE_COMPONENT_DIRECTORY}`, (err, files) => {
        if (!err) usedComponents = files.map(file => file.replace(/\.js$/, ""));
    });
}

// This function is simply used to retrieve the suggestions whenever they are out of date
// and update the intellisense completion items.

const getSuggestions = async () => {
    let code = await axios.get(`${SERVER}/suggestions`);

    components = code.data.storedCode; 

    if (suggestionDisposable) suggestionDisposable.dispose();

    suggestionDisposable = vscode.languages.registerCompletionItemProvider(['javascript', 'typescript'], {
        provideCompletionItems(doc, pos) {
            return createSuggestions(code.data.storedCode);
        }
    });
};

function createSuggestions ( items: {name:string, code: string}[]): CompletionItem[] {
    return items.map(item => {
        let newItem = new CompletionItem(`${GET_KEYWORD} `+item.name);
        return newItem;
    });
}

export function activate(context: vscode.ExtensionContext) {

    checkVersion();
    getUsedComponents();

    // Whenever a change happens in the document a quick check is done to see if 
    // a component was mentioned on the current line.

    // If a component name is found it is changed to fit the structure of a react component
    // and imported in the default react directory.


    // Because multiple changes can happen at a time, there can be conflicting update operations.
    // This noEdit boolean is set to false and stops any updates from changing until the
    // current change is done.

    let noEdit: boolean = true;
    
    vscode.window.onDidChangeTextEditorSelection((doc: TextEditorSelectionChangeEvent) => {
        let {line, character} = doc.selections[0].start;

        let {text} = doc.textEditor.document.lineAt(line);

        // Search the text to see if any react component is found.

        const checkForComponent = async (component) => {

            let indexFound: number = -1;

            // Continue if the component is not found

            if ( ( indexFound = text.indexOf(`${GET_KEYWORD} ${component.name}`) ) < 0)
                return;

            let edit: WorkspaceEdit = new WorkspaceEdit();
            let length: number = `${GET_KEYWORD} ${component.name}`.length;
            let range: Range = new Range(new Position(line, indexFound), new Position(line, indexFound + length));
            let pos: Position = new Position(0, 0);

            // Replace the GET {COMPONENT_NAME} to look like <{COMPONENT_NAME} />

            let props: string = "";

            if (component.propertyList.length > 0 )
                props = component.propertyList.map(prop => prop+'={null}')
                                            .reduce((previous, current) => current + ' '+previous);

            edit.replace(doc.textEditor.document.uri, range, `<${component.name} ${props} />`);

            // Next a check is made to see if there is an import for the component.

            // If it does not exist create one.

            const root = vscode.workspace.rootPath;
            const filePath = vscode.window.activeTextEditor.document.fileName;

            // I'm very unhappy with this part of code and would greatly appreciate it 
            // if someone wrote this in a better way.

            // I'm just making the path of the import statement relative to the file path
            // of the component and the one is in.

            let importDir = filePath.replace(root+"\\", "")
                                    .replace(/\\/g, "/")
                                    .replace(/[^\/]/g, ".")
                                    .replace(/\.+/g, "..")
                                    .substring(1);
            
            importDir = `${importDir}/${SAVE_COMPONENT_DIRECTORY}`;

            let foundIndex: number = -1;

            if ( (foundIndex = filePath.indexOf(SAVE_COMPONENT_DIRECTORY)) >= 0 ) {
                importDir = filePath.substring(foundIndex)
                                    .replace(/\\/g, "/")
                                    .replace(/[^\/]/g, ".")
                                    .replace(/\.+/g, "..")
                                    .substring(1)
                                    .replace(/\/\.\.$/, "");
            }

            const importPath = `import ${component.name} from '${importDir}/${component.name}';`;

            if ( doc.textEditor.document.getText().indexOf(importPath) < 0 )
                edit.insert(doc.textEditor.document.uri, pos, importPath+"\n");

            if (!noEdit) return; 

            noEdit = false;

            await vscode.workspace.applyEdit(edit);

            noEdit = true;

            // This next section then checks to see if the component is already stored 
            // inside the usedComponents list mentioned above.

            // If it is not mentioned it is added to the list and the file is created.

            if ( usedComponents.indexOf(component.name) < 0 ) {
                usedComponents.push(component.name);

                (async () => {
                    let code = await axios.get(`${SERVER}/component/${component.name}`);

                    const dir = `${vscode.workspace.rootPath}/${SAVE_COMPONENT_DIRECTORY}`;
                    const path = `${dir}/${component.name}.js`;
                
                    fs.exists(dir, (exists: boolean) =>  {

                        // As the fs library is callback based and node 8 has not been released 
                        // yet, this create file function is created here to avoid redundancy.

                        const createFile = () => fs.writeFile(path, code.data);

                        // A check if first required to see if the component directory exists yet.

                        // If it does not create it and then create the component, otherwise just create
                        // the component.

                        if (!exists)
                            fs.mkdir(dir, (err) => {
                                createFile();
                            });
                        else createFile();
                    });
                })();
            }
        };

        components.forEach(checkForComponent);
    });

    // This command is used to update a component file.

    // It simply looks at the file name to get the component name,
    // deletes all the current data,
    // and finally inserts the updated data retrieved from the server.

    let updateCommand: vscode.Disposable = vscode.commands.registerTextEditorCommand("reactComponentStore.update", 
    async (doc) => {
        const dir: string = doc.document.fileName;
        const filename: string = dir.substring( dir.lastIndexOf("\\") + 1, dir.lastIndexOf(".js") );

        let result = await axios.get(`${SERVER}/component/${filename}`);

        if (!result) return;

        let {document} = doc;
        let {lineCount, uri} = document;

        let edit: WorkspaceEdit = new WorkspaceEdit();
        let deleteRange: Range = new Range(new Position(0, 0), 
                                new Position(lineCount - 1, document.lineAt(lineCount - 1).text.length));

        edit.delete(uri, deleteRange);
        edit.insert(uri, new Position(0, 0), result.data);

        vscode.workspace.applyEdit(edit);

    });

    // The push command works pretty much exactly as the update commands does in terms of getting 
    // the file name and pushing all changes to a component with that name.

    // One difference however is that the push command allows you to select a section to use for the
    // push.

    // The reason for this is if there is some local code in a file one does not want to push.

    let pushCommand: vscode.Disposable = vscode.commands.registerTextEditorCommand("reactComponentStore.push",
        async (doc: vscode.TextEditor) => {
            var docLines: string[] = doc["_documentData"]["_lines"];

            const dir: string = doc.document.fileName;
            const componentName: string = dir.substring( dir.lastIndexOf("\\") + 1, dir.lastIndexOf(".js") );

            if (componentName && componentName.length > 0) {

                if (!doc.selection.isEmpty) {
                    var selectedLines = [];
            
                    const {start, end} = doc.selection;

                    selectedLines = docLines.slice(start.line, end.line + 1);

                    selectedLines[0] = docLines[ start.line ].substring(start.character);
                    selectedLines[ selectedLines.length - 1 ] = docLines[ end.line ].substring(0, end.character);

                    axios.post(`${SERVER}/push`, {componentName: componentName, docLines: selectedLines});
                } else {
                    axios.post(`${SERVER}/push`, {componentName, docLines});
                }

            }
    });

    context.subscriptions.push(pushCommand);
    context.subscriptions.push(updateCommand);
}



export function deactivate() {
}